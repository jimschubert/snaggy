using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Snaggy.Controllers;
using Snaggy.Data;
using Snaggy.Models;
using Xunit;

namespace Snaggy.Tests
{
    public class ApplicationControllerTest: IDisposable
    {
        private readonly JobApplicationContext _context;

        private readonly SolutionQuestion _question1 = new SolutionQuestion
        {
            Id = "id1000",
            Question = "What is the Earth's circumference in miles at the equator?",
            Answer = "24,874"
        };

        private readonly SolutionQuestion _question2 = new SolutionQuestion
        {
            Id = "id1001",
            Question = "What is the result of (1 << 2) >> 1?",
            Answer = "2"
        };

        private const string SubmitMessage = "Thank you.";

        public ApplicationControllerTest()
        {
            var builder = new DbContextOptionsBuilder<JobApplicationContext>();
            builder.UseInMemoryDatabase();
            var options = builder.Options;
            _context = new JobApplicationContext(options);

            // Add test applications data with random question data
            var applications = Enumerable.Range(1, 10)
                .Select(num => new JobApplication
                {
                    Name = $"User {num}",
                    Questions = new List<QuestionResponse>
                    {
                        new QuestionResponse
                        {
                            Answer = $"Answer: {num}",
                            Id = $"id{num}"
                        },
                        new QuestionResponse
                        {
                            Answer = $"Answer: {num + 1}",
                            Id = $"id{num + 1}"
                        }
                    },
                    Accepted = num % 2 == 0
                });
            _context.JobApplications.AddRange(applications);
            _context.SaveChanges();

            // Add test questions for validating "Accept" logic
            var questions = new List<SolutionQuestion>
            {
                _question1, _question2
            };
            _context.Questions.AddRange(questions);
            _context.SaveChanges();
        }

        [Fact]
        public async void TestGet()
        {
            // Arrange
            int expectedCount = 5; // only even apps are marked accepted
            int expectedQuestionCount = 2;
            var controller = new ApplicationController(_context);

            // Act
            var result = await controller.Get();
            var items = result.ToList();

            // Assert
            // List returned from endpoint
            Assert.Equal(expectedCount, items?.Count());
            // Each test data item has the same Questions count, so we can grab any here.
            Assert.Equal(expectedQuestionCount, items?.First()?.Questions?.Count);
        }

        [Fact]
        public async void TestGetApplication()
        {
            // Arrange
            // test data marks only even ids as accepted.
            int first = 1, second = 2;
            var controller = new ApplicationController(_context);

            // Act
            var firstApplication = await controller.GetApplication(first);
            var secondApplication = await controller.GetApplication(second);

            // Assert
            Assert.Null(firstApplication);
            Assert.NotNull(secondApplication);
        }

        [Fact]
        public async void TestSubmitValidQuestions()
        {
            // Arrange
            string expectedMessage = SubmitMessage;
            string applicationName = "Angelina Jolie";
            var controller = new ApplicationController(_context);

            var app = new SubmittedApplication
            {
                Name = applicationName,
                Questions = new List<QuestionResponse>
                {
                    new QuestionResponse
                    {
                        Id = _question1.Id,
                        Answer = _question1.Answer
                    },
                    new QuestionResponse
                    {
                        Id = _question2.Id,
                        Answer = _question2.Answer
                    }
                }
            };

            // Act
            var result = await controller.Submit(app);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(expectedMessage, result.Message);

            // Verify
            var inserted = _context.JobApplications.First(a => a.Name == applicationName);
            Assert.True(inserted.Accepted, "Inserted application with correct answers should have been accepted.");
        }

        [Fact]
        public async void TestSubmitInvalidQuestions()
        {
            // Arrange
            string expectedMessage = SubmitMessage;
            string applicationName = "Lisbeth Salander";
            var controller = new ApplicationController(_context);

            var app = new SubmittedApplication
            {
                Name = applicationName,
                Questions = new List<QuestionResponse>
                {
                    new QuestionResponse
                    {
                        Id = _question1.Id,
                        Answer = _question1.Answer
                    },
                    new QuestionResponse
                    {
                        Id = _question2.Id,
                        Answer = DateTime.Now.ToString("f") // Invalid answer
                    }
                }
            };

            // Act
            var result = await controller.Submit(app);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(expectedMessage, result.Message);

            // Verify
            var inserted = _context.JobApplications.First(a => a.Name == applicationName);
            Assert.False(inserted.Accepted, "Inserted application with any incorrect answers should NOT have been accepted.");
        }

        public void Dispose()
        {
            _context?.Database.EnsureDeleted();
            _context?.Dispose();
        }
    }
}