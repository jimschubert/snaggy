﻿using System.Collections.Generic;

namespace Snaggy.Models
{
    // NOTE: This class hides implementation details of JobApplication.
    // This can be handled elsehwere, but this is a quick and dirty solution.
    /// <summary>
    /// Represents a view model of an application
    /// </summary>
    public class SubmittedApplication
    {
        /// <summary>
        /// The name of the job application.
        /// TODO: Missing requirements about what "name" represents
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The list of user-submitted answers to questions on the application
        /// </summary>
        public List<QuestionResponse> Questions { get; set; }
    }
}