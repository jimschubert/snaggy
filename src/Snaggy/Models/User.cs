﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Snaggy.Models
{
    public class User : IdentityUser
    {
    }

    public class UserRole : IdentityRole
    {
        public UserRole()
        {
        }

        public UserRole(string roleName) : base(roleName)
        {
        }

        /// <summary>
        /// The role description
        /// </summary>
        public string Description { get; set; }
    }
}