﻿namespace Snaggy.Models
{
    /// <summary>
    /// Represents a common concept of a "question"
    /// </summary>
    public class SolutionQuestion
    {
        /// <summary>
        /// A string of characters which uniquely identifies a question
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// This is the text of the question.
        /// </summary>
        public string Question { get; set; }

        /// <summary>
        /// This is the acceptable answer for the question.
        /// TODO: In all likelyhood, there may be questions with one or more acceptable answers.
        ///       This should probably be a list.
        /// </summary>
        public string Answer { get; set; }
    }
}