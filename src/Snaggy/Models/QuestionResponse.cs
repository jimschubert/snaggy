﻿using Newtonsoft.Json;

namespace Snaggy.Models
{
    /// <summary>
    /// Represents a user-submited answer to a predefined question
    /// </summary>
    public class QuestionResponse
    {
        /// <summary>
        /// Unique identified for stored responses
        /// </summary>
        [JsonIgnore]
        public long QuestionResponseID { get; set; }

        /// <summary>
        /// The id of the existing question to which this response's answer is compared
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The applicant's answer to the question defined by <see cref="Id"/>
        /// </summary>
        public string Answer { get; set; }

        /// <summary>
        /// The unique identified for a job application
        /// </summary>
        [JsonIgnore]
        public long JobApplicationID { get; set; }

        /// <summary>
        /// The parent application
        /// </summary>
        [JsonIgnore]
        public JobApplication JobApplication { get; set; }
    }
}