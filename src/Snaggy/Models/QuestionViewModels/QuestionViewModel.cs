﻿namespace Snaggy.ViewModels
{
    public class QuestionViewModel
    {
        public string Id { get; set; }
        public string Question { get; set; }
    }
}