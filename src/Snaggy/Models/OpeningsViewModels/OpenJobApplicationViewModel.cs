﻿using System.Collections.Generic;
using Snaggy.ViewModels;

namespace Snaggy.Models.OpeningsViewModels
{
    public class OpenJobApplicationViewModel
    {
        public string Name { get; set; }
        public IEnumerable<QuestionViewModel> Questions { get; set; }
    }
}