﻿namespace Snaggy.Models.OpeningsViewModels
{
    public class OpeningsSummaryViewModel
    {
        /// <summary>
        /// A reference to the solution set for the opening's application
        /// </summary>
        public long SolutionID { get; set; }

        /// <summary>
        /// The name of the opening
        /// </summary>
        public string Name { get; set; }
    }
}