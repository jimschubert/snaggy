﻿namespace Snaggy.Models.ApplicationViewModels
{
    /// <summary>
    /// An output model which simply provides a message.
    /// </summary>
    public class SimpleMessageViewModel
    {
        /// <summary>
        /// A short message about an operation's outcome.
        /// </summary>
        public string Message { get; set; }
    }
}