﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Snaggy.Models
{
    /// <summary>
    /// Represents the user-submitted form of a job application
    /// </summary>
    public class JobApplication
    {
        /// <summary>
        /// The unique identified for a job application
        /// </summary>
        [JsonIgnore] // NOTE: ignored to meet requirements. Responses may not be entirely useful without a unique id to reference.
        public long JobApplicationID { get; set; }

        /// <summary>
        /// The name of the job application.
        /// TODO: Missing requirements about what "name" represents
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The list of user-submitted answers to questions on the application
        /// </summary>
        public virtual List<QuestionResponse> Questions { get; set; }

        [JsonIgnore]
        public bool Accepted { get; set; }
    }
}