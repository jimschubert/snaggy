﻿using System.Collections.Generic;

namespace Snaggy.Models
{
    /// <summary>
    /// Represents the solution set for a given job application
    /// TODO: Missing requirements. Obviously needed for cross-referencing with
    ///       acceptable answers. Class is based on assumptions about the workflow.
    /// </summary>
    public class Solution
    {
        /// <summary>
        /// Unique identified for this solution
        /// </summary>
        public long SolutionID { get; set; }

        /// <summary>
        /// The name of the solution set (or application). This is an assumption about the data representation.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The set of acceptable answers to the full solution
        /// </summary>
        public List<SolutionQuestion> SolutionSet { get; set; }
    }
}