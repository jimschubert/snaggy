﻿using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Snaggy.Auth
{
    /// <summary>
    /// Encapsulates the logic necessary to build parts of a JWT token
    /// </summary>
    internal class TokenDetails
    {
        /// <summary>
        /// The token endpoint
        /// </summary>
        internal string Path { get; set; }

        /// <summary>
        /// The JWT iss key
        /// </summary>
        internal string Issuer { get; set; }

        /// <summary>
        /// The JWT aud key
        /// </summary>
        internal string Audience { get; set; }

        /// <summary>
        /// The signing key used to protect the JWT token
        /// </summary>
        internal SymmetricSecurityKey SigningKey { get; set; }

        /// <summary>
        /// The credentials. For JWT, this is HMAC 256
        /// </summary>
        internal SigningCredentials SigningCredentials { get; set; }

        /// <summary>
        /// Constructs a new instance of <see cref="TokenDetails"/>
        /// </summary>
        /// <param name="configuration">Config</param>
        internal TokenDetails(IConfiguration configuration)
        {
            Path = configuration.GetSection("jwt:path").Value;
            Audience = configuration.GetSection("jwt:audience").Value;
            Issuer = configuration.GetSection("jwt:issuer").Value;

            SigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(configuration.GetSection("jwt:secret").Value));
            SigningCredentials = new SigningCredentials(SigningKey, SecurityAlgorithms.HmacSha256);
        }
    }
}