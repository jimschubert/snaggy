﻿// ReSharper disable InconsistentNaming
namespace Snaggy.Auth
{
    /// <summary>
    /// Application-specific auth token response object
    /// </summary>
    public class TokenResponse
    {
        /// <summary>
        /// The JWT token string
        /// </summary>
        public string access_token { get; set; }

        /// <summary>
        /// TTL in seconds
        /// </summary>
        public int expires_in { get; set; }
    }
}