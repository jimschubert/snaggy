﻿using System.Collections.Generic;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Snaggy.Auth
{
    /// <summary>
    /// Applies /token endpoint to existing swagger doc. Necessary because /token is exposed via third-party middleware,
    /// and not written to ApiExplorer (where Swashbuckle reads endpoints)
    /// </summary>
    class AuthTokenSwaggerOperation : IDocumentFilter
    {
        public void Apply(SwaggerDocument swaggerDoc, DocumentFilterContext context)
        {
            swaggerDoc.Paths.Add("/token", new PathItem
            {
                Post = new Operation
                {
                    Tags = new List<string> {"Auth"},
                    Consumes = new List<string>
                    {
                        " multipart/form-data"
                    },
                    Parameters = new List<IParameter>
                    {
                        new NonBodyParameter
                        {
                            Type = "string",
                            Name = "username",
                            Required = true,
                            In = "formData"
                        },
                        new NonBodyParameter
                        {
                            Type = "string",
                            Name = "password",
                            Required = true,
                            In = "formData"
                        }
                    },
                    Responses = new Dictionary<string, Response>
                    {
                        {
                            "200", new Response
                            {
                                Description = "Auth token result",
                                Schema = new Schema
                                {
                                    Type = "object",
                                    Description = "OAuth2 compatible token result",
                                    Required = new List<string> {"access_token", "expires_in"},
                                    Properties = new Dictionary<string, Schema>
                                    {
                                        {
                                            "access_token",
                                            new Schema {Type = "string", Description = "The JWT access token"}
                                        },
                                        {
                                            "expires_in",
                                            new Schema
                                            {
                                                Type = "integer",
                                                Format = "int32",
                                                Description = "The number of seconds in which the token expires"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }
    }
}