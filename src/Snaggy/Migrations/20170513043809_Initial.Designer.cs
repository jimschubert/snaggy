﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Snaggy.Data;

namespace Snaggy.Migrations
{
    [DbContext(typeof(JobApplicationContext))]
    [Migration("20170513043809_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Snaggy.Models.JobApplication", b =>
                {
                    b.Property<long>("JobApplicationID")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Accepted");

                    b.Property<string>("Name");

                    b.HasKey("JobApplicationID");

                    b.ToTable("JobApplications");
                });

            modelBuilder.Entity("Snaggy.Models.QuestionResponse", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Answer");

                    b.Property<long>("JobApplicationID");

                    b.HasKey("Id");

                    b.HasIndex("JobApplicationID");

                    b.ToTable("QuestionResponse");
                });

            modelBuilder.Entity("Snaggy.Models.Solution", b =>
                {
                    b.Property<long>("SolutionID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("SolutionID");

                    b.ToTable("Solutions");
                });

            modelBuilder.Entity("Snaggy.Models.SolutionQuestion", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Answer");

                    b.Property<string>("Question");

                    b.Property<long?>("SolutionID");

                    b.HasKey("Id");

                    b.HasIndex("SolutionID");

                    b.ToTable("Questions");
                });

            modelBuilder.Entity("Snaggy.Models.QuestionResponse", b =>
                {
                    b.HasOne("Snaggy.Models.JobApplication", "JobApplication")
                        .WithMany("Questions")
                        .HasForeignKey("JobApplicationID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Snaggy.Models.SolutionQuestion", b =>
                {
                    b.HasOne("Snaggy.Models.Solution")
                        .WithMany("SolutionSet")
                        .HasForeignKey("SolutionID");
                });
        }
    }
}
