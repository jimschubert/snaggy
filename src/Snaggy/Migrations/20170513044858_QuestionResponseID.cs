﻿using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Snaggy.Migrations
{
    public partial class QuestionResponseID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_QuestionResponse",
                table: "QuestionResponse");

            migrationBuilder.AlterColumn<string>(
                name: "Id",
                table: "QuestionResponse",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<long>(
                name: "QuestionResponseID",
                table: "QuestionResponse",
                nullable: false,
                defaultValue: 0L)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_QuestionResponse",
                table: "QuestionResponse",
                column: "QuestionResponseID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_QuestionResponse",
                table: "QuestionResponse");

            migrationBuilder.DropColumn(
                name: "QuestionResponseID",
                table: "QuestionResponse");

            migrationBuilder.AlterColumn<string>(
                name: "Id",
                table: "QuestionResponse",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_QuestionResponse",
                table: "QuestionResponse",
                column: "Id");
        }
    }
}
