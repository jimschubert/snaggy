﻿using System;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.IdentityModel.Tokens;
using Snaggy.Auth;
using Snaggy.Data;
using Snaggy.Models;
using Snaggy.Services;
using Swashbuckle.AspNetCore.Swagger;

namespace Snaggy
{
    public class Startup
    {
        public IHostingEnvironment Env { get; set; }
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

            Env = env;
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();
            // Setup swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info {Title = "Snaggy", Version = "v1"});

                //Set the comments path for the swagger json and ui.
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "comments.xml");
                c.IncludeXmlComments(xmlPath);

                // This adds /token to Swagger documentation
                c.DocumentFilter<AuthTokenSwaggerOperation>();
            });

            services.AddDbContext<JobApplicationContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddDbContext<IdentityContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddIdentity<User, UserRole>()
                .AddEntityFrameworkStores<IdentityContext>()
                .AddDefaultTokenProviders();

            // Allows access to configuration in other classes.
            services.AddSingleton<IConfiguration>(Configuration);
            services.AddSingleton(new TokenDetails(Configuration));

            // Add application services.
            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();

            services.AddAuthorization(auth =>
            {
                auth.AddPolicy("Bearer", new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser().Build());
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app,
            IHostingEnvironment env,
            ILoggerFactory loggerFactory,
            JobApplicationContext context,
            IdentityContext identityContext,
            UserManager<User> userManager,
            RoleManager<UserRole> roleManager,
            SignInManager<User> signInManager
        )
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseStaticFiles();

            app.UseIdentity();
            var tokenDetails = new TokenDetails(Configuration);
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = false,
                ValidateAudience = true,
                ValidAudience = Configuration.GetSection("jwt:audience").Value,
                ValidateLifetime = true,
                IssuerSigningKey = tokenDetails.SigningKey,
                ClockSkew = TimeSpan.FromMinutes(0)
            };

            var tokenProviderOptions = new TokenProviderOptions
            {
                Path = tokenDetails.Path,
                Audience = tokenDetails.Audience,
                Issuer = tokenDetails.Issuer,
                SigningCredentials = tokenDetails.SigningCredentials,
                IdentityResolver = async (username, password) =>
                {
                    var user = await userManager.FindByNameAsync(username);
                    var result = await signInManager.CheckPasswordSignInAsync(user, password, lockoutOnFailure: false);

                    if (!result.Succeeded) return null;

                    var principal = await signInManager.CreateUserPrincipalAsync(user);
                    return new ClaimsIdentity(principal.Identity, principal.Claims);
                }
            };

            var jwtOptions = new JwtBearerOptions
            {
                Audience = tokenDetails.Audience,
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                TokenValidationParameters = tokenValidationParameters
            };

            app.UseJwtBearerAuthentication(jwtOptions);

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                AuthenticationScheme = "Cookie",
                CookieName = "access_token",
                TicketDataFormat = new CustomJwtDataFormat(
                    SecurityAlgorithms.HmacSha256,
                    tokenValidationParameters)
            });

            app.UseMiddleware<TokenProvider>(Options.Create(tokenProviderOptions));

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Snaggy API V1");
            });

            DbInitializer.Initialize(context, identityContext, userManager, roleManager);

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
