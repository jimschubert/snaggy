﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Snaggy.Data;
using Snaggy.Models;
using Snaggy.Models.OpeningsViewModels;
using Snaggy.ViewModels;

namespace Snaggy.Controllers
{
    /// <summary>
    /// Actions related to summary details for potential jobs.
    /// </summary>
    [Route("api/[controller]")]
    public class OpeningsController : Controller
    {
        private readonly JobApplicationContext _context;

        /// <summary>
        /// Constructs a new instance of <see cref="OpeningsController"/>.
        /// </summary>
        /// <param name="context">EF context</param>
        public OpeningsController(JobApplicationContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Retrieves a summary detail of all available openings.
        /// </summary>
        /// <response code="200">The list of openings.</response>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<OpeningsSummaryViewModel>), 200)]
        public async Task<IEnumerable<OpeningsSummaryViewModel>> Get()
        {
            return await _context.Solutions.Select(s =>
                new OpeningsSummaryViewModel { SolutionID =  s.SolutionID, Name = s.Name}
            ).ToListAsync();
        }

        /// <summary>
        /// Gets a job application for an opening by id.
        /// </summary>
        /// <param name="jobId">The id of the job's questionnaire.</param>
        /// <response code="200">The job opening's set of questions</response>
        [HttpGet("{jobId}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(OpenJobApplicationViewModel), 200)]
        public async Task<OpenJobApplicationViewModel> GetById(long jobId)
        {
            var solution = await _context.Solutions
                .Include(s => s.SolutionSet)
                .FirstOrDefaultAsync(s => s.SolutionID == jobId);

            if (solution == null) return null;

            return new OpenJobApplicationViewModel
            {
                Name = solution.Name,
                Questions = solution.SolutionSet.Select(s => new QuestionViewModel {Id = s.Id, Question = s.Question})
            };
        }

        /// <summary>
        /// Gets all questions for a given job by id.
        /// </summary>
        /// <remarks>
        /// This is an example of a nested resource. It is not intended to be used currently, because
        /// <see cref="GetById"/> would only return an object with a single name parameter!
        /// </remarks>
        /// <param name="jobId"></param>
        /// <response code="200">A list of questions for a job's application</response>
        [HttpGet("{jobId}/questions")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<QuestionViewModel>), 200)]
        public async Task<IEnumerable<QuestionViewModel>> GetQuestionsBySolutionId(long jobId)
        {
            return await _context.Solutions.Where(s => s.SolutionID == jobId).Select(s => s.SolutionSet).SelectMany(q => q.Select(s => new QuestionViewModel
            {
                Id = s.Id,
                Question = s.Question
            })).ToListAsync();
        }
    }
}