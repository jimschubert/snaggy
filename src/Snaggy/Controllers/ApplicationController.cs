﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Snaggy.Data;
using Snaggy.Models;
using Snaggy.Models.ApplicationViewModels;

namespace Snaggy.Controllers
{
    /// <summary>
    /// Contains actions related to job applications (submit, view, etc)
    /// </summary>
    [Route("api/[controller]")]
    [Produces("application/json")]
    [Authorize]
    public class ApplicationController : Controller
    {
        private readonly JobApplicationContext _context;

        /// <summary>
        /// Constructs a new instance of <see cref="ApplicationController"/>
        /// </summary>
        /// <param name="context">EF context</param>
        public ApplicationController(JobApplicationContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Retrieves all applications with 100% correct answers.
        /// </summary>
        /// <response code="200">Accepted applications</response>
        [HttpGet]
        [Authorize(Roles="Admin")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<JobApplication>), 200)]
        public async Task<IEnumerable<JobApplication>> Get()
        {
            // Questions(application responses) are included here per requirement. Assume this is in case questions change.
            return await _context.JobApplications.Where(a => a.Accepted).Include("Questions").ToListAsync();
        }

        /// <summary>
        /// Gets an accepted application by id
        /// </summary>
        /// <param name="id">The submitted application's id</param>
        /// <response code="200">Accepted application</response>
        [HttpGet("{id}")]
        [Authorize(Roles="Admin")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(JobApplication), 200)]
        public async Task<JobApplication> GetApplication(int id)
        {
            return await _context.JobApplications.FirstOrDefaultAsync(a => a.JobApplicationID == id && a.Accepted);
        }

        /// <summary>
        /// Accepts a job application for manager review.
        /// </summary>
        /// <param name="submittedApplication">The contents of a job application's questionnaire.</param>
        /// <response code="200">Application was accepted</response>
        [HttpPost]
        [Produces("application/json")]
        [ProducesResponseType(typeof(SimpleMessageViewModel), 200)]
        public async Task<SimpleMessageViewModel> Submit([FromBody] SubmittedApplication submittedApplication)
        {
            const string responseMessage = "Thank you.";

            if (submittedApplication?.Questions == null || submittedApplication.Questions.Count == 0)
            {
                // There's no requirement here about what system is submitting the data (directly from a user, or another system?)
                // For a purely RESTful API, this would be a combination of HTTP Status code and message, preferrably following
                // Nottingham's IETF draft for HTTP Problem Details
                return new SimpleMessageViewModel
                {
                    Message = responseMessage
                };
            }

            var application = new JobApplication
            {
                Name = submittedApplication.Name,
                Questions = submittedApplication.Questions
            };

            // Immediate grading, as there's no requirement for batching on process-bound externalized grading
            var keys = application.Questions.Select(q => q.Id).ToArray();
            var answers = await _context.Questions.Where(q => keys.Contains(q.Id))
                .ToDictionaryAsync(quesiton => quesiton.Id, question => question);

            // Ideally, we'd probably want to use SOUNDEX to compare answers here, but it's not a current requirement.
            application.Accepted =
                application.Questions.TrueForAll(r => answers.ContainsKey(r.Id) && answers[r.Id].Answer == r.Answer);

            // There are two routes we could go here. Either just not save applications that incorrectly answer questions
            // or save all, even if we will only query those with Accepted=true. I prefer the latter to avoid legal issues.
            // Audit fields could be applied and data could be cleaned up periodically in the future.
            _context.JobApplications.Add(application);
            await _context.SaveChangesAsync();

            // see comment above about HTTP Status Code and Nottingham's IETF draft
            return new SimpleMessageViewModel
            {
                Message = responseMessage
            };
        }
    }
}
