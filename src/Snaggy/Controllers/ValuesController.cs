﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Snaggy.Controllers
{
    /// <summary>
    /// A reuse of the auto-generated controller from a default .NET Core project.
    /// </summary>
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        /// <summary>
        /// Admin role which displays all roles for the user.
        /// </summary>
        /// <remarks>Useful for debugging</remarks>
        /// <response code="200">An array of dynamic claim type/value objects.</response>
        [Authorize(Roles="Admin")]
        [HttpGet("claims")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(object), 200)]
        public object Claims()
        {
            return User.Claims.Select(c =>
                new
                {
                    c.Type,
                    c.Value
                });
        }
    }
}
