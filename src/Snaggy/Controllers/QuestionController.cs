﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Snaggy.Data;
using Snaggy.Models;
using Snaggy.ViewModels;

namespace Snaggy.Controllers
{
    /// <summary>
    /// Actions related to questions only.
    /// This set of operations is for example only, via Swagger.
    /// Admin-only.
    /// </summary>
    [Route("api/[controller]")]
    [Authorize(Roles = "Admin")]
    public class QuestionsController : Controller
    {
        private readonly JobApplicationContext _context;

        /// <summary>
        /// Contructs a new instance of <see cref="QuestionsController"/>
        /// </summary>
        /// <param name="context">EF context</param>
        public QuestionsController(JobApplicationContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Gets all questions in the system, regardless of job.
        /// </summary>
        /// <response code="200">All questions, without answers</response>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<QuestionViewModel>), 200)]
        public async Task<IEnumerable<QuestionViewModel>> Get()
        {
            return await _context.Questions.Select(q => new QuestionViewModel
            {
                Id = q.Id,
                Question = q.Question
            }).ToListAsync();
        }

        /// <summary>
        /// Gets a single question by id.
        /// </summary>
        /// <param name="questionId">The identifier of the question.</param>
        /// <response code="200">The question, without answer</response>
        [HttpGet("{questionId}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(QuestionViewModel), 200)]
        public async Task<QuestionViewModel> GetById(string questionId)
        {
            var question = await _context.Questions.FirstOrDefaultAsync(q => q.Id == questionId);

            if(question == null) return null;

            return new QuestionViewModel
            {
                Id = question.Id,
                Question = question.Question
            };
        }
    }
}