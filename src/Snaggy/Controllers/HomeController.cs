﻿using Microsoft.AspNetCore.Mvc;

namespace Snaggy.Controllers
{
    public class HomeController : Controller
    {
        // GET
        public ActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Don't just land any job.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "james.schubert@gmail.com";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}