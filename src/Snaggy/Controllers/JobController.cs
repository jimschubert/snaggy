﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Snaggy.Data;

namespace Snaggy.Controllers
{
    /// <summary>
    /// Presents Job-related views to users
    /// </summary>
    [Authorize]
    [Route("[controller]")]
    public class JobController : Controller
    {
        private readonly JobApplicationContext _context;

        /// <summary>
        /// Creates an instance of <see cref="JobController"/>
        /// </summary>
        /// <param name="context">EF context</param>
        public JobController(JobApplicationContext context)
        {
            _context = context;
        }

        // NOTE: jobId here is solutionId
        /// <summary>
        /// Retrieves an HTML view of the job application for a potential candidate to fill out.
        /// </summary>
        /// <param name="jobId">The intended job's id.</param>
        /// <response code="200">HTML output</response>
        /// <response code="302">HTML redirect when not found</response>
        [HttpGet("[action]/{id:long}")]
        public async Task<IActionResult> Apply([FromRoute(Name = "id")]long jobId)
        {
            var applicationQuestions = await _context.Solutions.Include(s => s.SolutionSet).FirstOrDefaultAsync(j => j.SolutionID == jobId);
            if (null == applicationQuestions)
            {
                ViewData["Message"] = "That job was not found.";
                return RedirectToAction("Index", "Home");
            }

            return View(applicationQuestions);
        }
    }
}