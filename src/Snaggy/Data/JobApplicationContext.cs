﻿using Microsoft.EntityFrameworkCore;
using Snaggy.Models;

namespace Snaggy.Data
{
    public class JobApplicationContext : DbContext
    {
        protected JobApplicationContext()
        {
        }

        public JobApplicationContext(DbContextOptions<JobApplicationContext> options) : base(options)
        {
        }

        /// <summary>
        /// Database access to job applications
        /// </summary>
        public DbSet<JobApplication> JobApplications { get; set; }

        /// <summary>
        /// Database access to questions
        /// </summary>
        public DbSet<SolutionQuestion> Questions { get; set; }

        /// <summary>
        /// Database access to solution sets
        /// </summary>
        public DbSet<Solution> Solutions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }
    }
}