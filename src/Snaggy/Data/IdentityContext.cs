﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Snaggy.Models;

namespace Snaggy.Data
{
    public class IdentityContext: IdentityDbContext<User, UserRole, string>
    {
        public IdentityContext(DbContextOptions options) : base(options)
        {
        }
    }
}