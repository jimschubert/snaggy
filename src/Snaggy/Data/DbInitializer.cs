﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Snaggy.Auth;
using Snaggy.Models;

namespace Snaggy.Data
{
    public static class DbInitializer
    {
        /// <summary>
        /// Defines programmatic data bootstrapping. I'm using this for test data, but in reality, this would include only critical application runtime data.
        /// NOTE: There are other ways to do this in Entity Framework, but this is a quick, clean and refactorable simplification.
        /// </summary>
        /// <param name="context">Application data</param>
        /// <param name="identityContext">Security related data</param>
        /// <param name="userManager">Framework implementation for managing users</param>
        /// <param name="roleManager">Framework implementation for managing roles</param>
        /// <exception cref="InvalidOperationException"></exception>
        public static void Initialize(
            JobApplicationContext context,
            IdentityContext identityContext,
            UserManager<User> userManager,
            RoleManager<UserRole> roleManager)
        {
            // NOTE: identityContext.Database.EnsureCreated() will cause future migrations to break, so we migrate if outstanding.
            if(identityContext.Database.GetPendingMigrations().Any()) identityContext.Database.Migrate();
            if(context.Database.GetPendingMigrations().Any()) context.Database.Migrate();

            if (!context.Solutions.Any())
            {
                var solutionQuestions = new[]
                {
                    new SolutionQuestion {Id = "id1", Question = "What is 3 minus 15?", Answer = "-12"},
                    new SolutionQuestion {Id = "id2",  Question = "How many US ounces in a US pound?", Answer = "16"},
                    new SolutionQuestion {Id = "id3",  Question = "Which is more common, UTF8 or UTF16?", Answer = "UTF8"},
                    new SolutionQuestion {Id = "id4",  Question = "Who was the first US President?", Answer = "George Washington"},
                    new SolutionQuestion {Id = "id5",  Question = "What git command shows a log of historical branch switches?", Answer = "reflog"}
                };

                context.Questions.AddRange(solutionQuestions);
                context.SaveChanges();

                var solutions = new[]
                {
                    new Solution
                    {
                        Name = "Junior Software Engineer",
                        SolutionSet = new List<SolutionQuestion> { solutionQuestions[0], solutionQuestions[1] }
                    },
                    new Solution
                    {
                        Name = "Senior Software Engineer",
                        SolutionSet = new List<SolutionQuestion> { solutionQuestions[2], solutionQuestions[3], solutionQuestions[4] }
                    }
                };
                context.Solutions.AddRange(solutions);
                context.SaveChanges();
            }

            if (!identityContext.Users.Any())
            {
                var normalUser = new User {UserName = "user@example.com", Email = "user@example.com"};
                var userRole = new UserRole("User") {Description = "Normal User"};
                var admin = new User {UserName = "admin@example.com", Email = "admin@example.com"};
                var adminRole = new UserRole("Admin") {Description = "Administrator"};

                var result = userManager.CreateAsync(normalUser, "AtestUser^1").Result;
                if(false == result.Succeeded) throw new InvalidOperationException("DbInitializer failed to create normal user account.");

                result = userManager.CreateAsync(admin,"AtestUser^1").Result;
                if(false == result.Succeeded) throw new InvalidOperationException("DbInitializer failed to create administrator account.");

                var roleResult = roleManager.CreateAsync(adminRole).Result;
                if(false == roleResult.Succeeded) throw new InvalidOperationException("DbInitializer failed to create administrator role.");

                roleResult = roleManager.CreateAsync(userRole).Result;
                if(false == roleResult.Succeeded) throw new InvalidOperationException("DbInitializer failed to create normal user role.");

                var identityResult = userManager.AddToRoleAsync(admin, adminRole.Name).Result;
                if (false == identityResult.Succeeded)  throw new InvalidOperationException("DbInitializer failed to assign administrator the admin role.");

                identityResult = userManager.AddToRoleAsync(admin, userRole.Name).Result;
                if (false == identityResult.Succeeded)  throw new InvalidOperationException("DbInitializer failed to assign administrator the user role.");

                identityResult = userManager.AddToRoleAsync(normalUser, userRole.Name).Result;
                if (false == identityResult.Succeeded)  throw new InvalidOperationException("DbInitializer failed to assign normal user the user role.");
            }
        }
    }
}