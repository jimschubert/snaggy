#!/bin/bash
# Helper script to ensure custom docker network. Args are passed to docker-compose up

# Standard bash options which can't fit in the preamble becuase there's more than one option
# From `help set`
# -e  Exit immediately if a command exits with a non-zero status.
# -u Treat unset variables as an error when substituting.
# -o pipefail the return value of a pipeline is the status of
#    the last command to exit with a non-zero status,
#    or zero if no command exited with a non-zero status
set -euo pipefail

COMPOSE_ARGS="${@:-up -d --no-recreate --build}"

\docker-compose ${COMPOSE_ARGS}
